package stu.elasticsearch.jdbc;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/7 13:25
 */
public class EsResultSetMetaData implements ResultSetMetaData, EsWrapper {

    private final EsResultSet esResultSet;

    public EsResultSetMetaData(EsResultSet esResultSet) {
        this.esResultSet = esResultSet;
    }

    @Override
    public int getColumnCount() throws SQLException {
        return esResultSet.getKeyList().size();
    }

    @Override
    public boolean isAutoIncrement(int column) throws SQLException {
        return false;
    }

    @Override
    public boolean isCaseSensitive(int column) throws SQLException {
        return true;
    }

    @Override
    public boolean isSearchable(int column) throws SQLException {
        return true;
    }

    @Override
    public boolean isCurrency(int column) throws SQLException {
        return false;
    }

    @Override
    public int isNullable(int column) throws SQLException {
        return 2;
    }

    @Override
    public boolean isSigned(int column) throws SQLException {
        return TypeUtils.isSigned(esResultSet.getTypeList().get(column - 1));
    }

    @Override
    public int getColumnDisplaySize(int column) throws SQLException {
        return esResultSet.getKeyList().size();
    }

    @Override
    public String getColumnLabel(int column) throws SQLException {
        return esResultSet.getKeyList().get(column - 1);
    }

    @Override
    public String getColumnName(int column) throws SQLException {
        return esResultSet.getKeyList().get(column - 1);
    }

    @Override
    public String getSchemaName(int column) throws SQLException {
        return "";
    }

    @Override
    public int getPrecision(int column) throws SQLException {
        return 0;
    }

    @Override
    public int getScale(int column) throws SQLException {
        return 0;
    }

    @Override
    public String getTableName(int column) throws SQLException {
        return "";
    }

    @Override
    public String getCatalogName(int column) throws SQLException {
        return "";
    }

    @Override
    public int getColumnType(int column) throws SQLException {
        return esResultSet.getTypeList().get(column - 1).getVendorTypeNumber().intValue();
    }

    @Override
    public String getColumnTypeName(int column) throws SQLException {
        return esResultSet.getTypeList().get(column - 1).getName();
    }

    @Override
    public boolean isReadOnly(int column) throws SQLException {
        return true;
    }

    @Override
    public boolean isWritable(int column) throws SQLException {
        return false;
    }

    @Override
    public boolean isDefinitelyWritable(int column) throws SQLException {
        return false;
    }

    @Override
    public String getColumnClassName(int column) throws SQLException {
        return TypeUtils.classOf(esResultSet.getTypeList().get(column - 1)).getName();
    }

}
