package stu.elasticsearch.jdbc;

import java.sql.SQLException;
import java.sql.Wrapper;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/8 9:26
 */
public interface EsWrapper extends Wrapper {

    default boolean isWrapperFor(Class<?> iface) throws SQLException {
        return (iface != null && iface.isAssignableFrom(getClass()));
    }

    default <T> T unwrap(Class<T> iface) throws SQLException {
        if (isWrapperFor(iface))
            return (T) this;
        throw new SQLException();
    }
}
