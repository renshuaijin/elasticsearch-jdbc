package stu.elasticsearch.jdbc;

import java.sql.SQLType;

public enum EsType implements SQLType {
    NULL(0),
    UNSUPPORTED(1111),
    BOOLEAN(16),
    BYTE(-6),
    SHORT(5),
    INTEGER(4),
    LONG(-5),
    DOUBLE(8),
    FLOAT(7),
    HALF_FLOAT(6),
    SCALED_FLOAT(6),
    KEYWORD(12),
    TEXT(12),
    OBJECT(2002),
    NESTED(2002),
    BINARY(-3),
    DATE(91),
    TIME(92),
    DATETIME(93),
    IP(12),
    INTERVAL_YEAR(101),
    INTERVAL_MONTH(102),
    INTERVAL_YEAR_TO_MONTH(107),
    INTERVAL_DAY(103),
    INTERVAL_HOUR(104),
    INTERVAL_MINUTE(105),
    INTERVAL_SECOND(106),
    INTERVAL_DAY_TO_HOUR(108),
    INTERVAL_DAY_TO_MINUTE(109),
    INTERVAL_DAY_TO_SECOND(110),
    INTERVAL_HOUR_TO_MINUTE(111),
    INTERVAL_HOUR_TO_SECOND(112),
    INTERVAL_MINUTE_TO_SECOND(113),
    GEO_POINT(114),
    GEO_SHAPE(114),
    SHAPE(114);

    private final Integer type;

    EsType(int type) {
        this.type = Integer.valueOf(type);
    }

    public String getName() {
        return name();
    }

    public String getVendor() {
        return "stu.elasticsearch";
    }

    public Integer getVendorTypeNumber() {
        return this.type;
    }
}
