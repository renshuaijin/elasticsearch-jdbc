package stu.elasticsearch.jdbc;

import java.sql.ParameterMetaData;
import java.sql.SQLException;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/7 13:20
 */
public class EsParameterMetaData implements ParameterMetaData, EsWrapper {
    private final EsPreparedStatement esPreparedStatement;

    public EsParameterMetaData(EsPreparedStatement esPreparedStatement) {
        this.esPreparedStatement = esPreparedStatement;
    }

    public int getParameterCount() throws SQLException {
        return 0;
    }

    public int isNullable(int param) throws SQLException {
        return 2;
    }

    public boolean isSigned(int param) throws SQLException {
        return true;
    }

    public int getPrecision(int param) throws SQLException {
        return 0;
    }

    public int getScale(int param) throws SQLException {
        return 0;
    }

    public int getParameterType(int param) throws SQLException {
        return 0;
    }

    public String getParameterTypeName(int param) throws SQLException {
        return null;
    }

    public String getParameterClassName(int param) throws SQLException {
        return null;
    }

    public int getParameterMode(int param) throws SQLException {
        return 0;
    }

}
