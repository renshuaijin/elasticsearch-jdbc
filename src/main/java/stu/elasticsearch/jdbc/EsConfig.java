package stu.elasticsearch.jdbc;

import sun.misc.BASE64Encoder;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/8 9:46
 */
public class EsConfig {
    private String jdbcUrl;
    private String url;
    private String username;
    private String password;
    private String auth;
    private int connectTimeout;
    private int readTimeout;


    public EsConfig(String jdbcUrl, Properties props) {
        init(jdbcUrl, props);
    }

    private void init(String jdbcUrl, Properties props) {
        this.jdbcUrl = jdbcUrl;
        this.url = (jdbcUrl.contains("http") ? "" : "http://")
                + (jdbcUrl.replaceFirst(EsTool.URL_PREFIX, "") + "/").replace("//", "/")
                + "_sql?format=json";
        // BASE64加密
        this.username = props.getProperty("user", props.getProperty("username"));
        this.password = props.getProperty("pass", props.getProperty("password"));
        this.auth = "Basic " + new BASE64Encoder().encode((username + ":" + password).getBytes(StandardCharsets.UTF_8));

        this.connectTimeout = EsTool.parseInt(props.getProperty("connectTimeout"), 30000);
        this.readTimeout = EsTool.parseInt(props.getProperty("readTimeout"), 60000);
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }
}
