package stu.elasticsearch.jdbc;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/6 18:19
 */
public class EsDriver implements Driver {

    static {
        try {
            DriverManager.registerDriver(new EsDriver());
        } catch (SQLException e) {
            throw new RuntimeException("Can't register driver!");
        }
    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        if (url == null) {
            throw new SQLException("Non-null url required");
        }
        if (!acceptsURL(url)) {
            return null;
        }
        EsConfig esConfig = new EsConfig(url, info);
        return new EsConnection(esConfig);
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        if (EsTool.canAccept(url)) {
            return true;
        }
        throw new SQLException("Expected [jdbc:es://] url, received [" + url + "]");
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        return new DriverPropertyInfo[0];
    }

    @Override
    public int getMajorVersion() {
        return 0;
    }

    @Override
    public int getMinorVersion() {
        return 0;
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
