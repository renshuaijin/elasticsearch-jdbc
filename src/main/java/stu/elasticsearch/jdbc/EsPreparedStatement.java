package stu.elasticsearch.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Properties;

/**
 * 一句简洁的说明
 *
 * @author lzy
 * @date 2021/10/6 20:08
 */
public class EsPreparedStatement extends EsStatement implements PreparedStatement {

    private final EsConnection esConnection;
    private final EsHttpClient esHttpClient;
    private final String sql;

    public EsPreparedStatement(EsConnection esConnection, EsConfig esConfig, String sql) throws SQLException {
        super(esConnection, esConfig);
        this.esConnection = esConnection;
        this.esHttpClient = new EsHttpClient(esConfig);
        this.sql = sql;
    }


    public boolean execute() throws SQLException {
        this.executeQuery();
        return true;
    }

    public ResultSet executeQuery() throws SQLException {
        this.initResultSet(sql);
        return this.esResultSet;
    }

    public int executeUpdate() throws SQLException {
        throw new SQLFeatureNotSupportedException("Writes not supported");
    }

    public void setNull(int parameterIndex, int sqlType) throws SQLException {
    }

    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
    }

    public void setByte(int parameterIndex, byte x) throws SQLException {
    }

    public void setShort(int parameterIndex, short x) throws SQLException {
    }

    public void setInt(int parameterIndex, int x) throws SQLException {
    }

    public void setLong(int parameterIndex, long x) throws SQLException {
    }

    public void setFloat(int parameterIndex, float x) throws SQLException {
    }

    public void setDouble(int parameterIndex, double x) throws SQLException {
    }

    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
    }

    public void setString(int parameterIndex, String x) throws SQLException {
    }

    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
    }

    public void setDate(int parameterIndex, Date x) throws SQLException {
    }

    public void setTime(int parameterIndex, Time x) throws SQLException {
    }

    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
    }

    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new SQLFeatureNotSupportedException("AsciiStream not supported");
    }

    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new SQLFeatureNotSupportedException("UnicodeStream not supported");
    }

    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new SQLFeatureNotSupportedException("BinaryStream not supported");
    }

    public void clearParameters() throws SQLException {
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
    }

    public void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
    }

    public void setObject(int parameterIndex, Object x) throws SQLException {
    }

    public void addBatch() throws SQLException {
        throw new SQLFeatureNotSupportedException("Batching not supported");
    }

    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
        throw new SQLFeatureNotSupportedException("CharacterStream not supported");
    }

    public void setRef(int parameterIndex, Ref x) throws SQLException {
    }

    public void setBlob(int parameterIndex, Blob x) throws SQLException {
    }

    public void setClob(int parameterIndex, Clob x) throws SQLException {
    }

    public void setArray(int parameterIndex, Array x) throws SQLException {
    }

    public ResultSetMetaData getMetaData() throws SQLException {
        return null;
    }

    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
    }

    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
    }

    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
    }

    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
    }

    public void setURL(int parameterIndex, URL x) throws SQLException {
    }

    public ParameterMetaData getParameterMetaData() throws SQLException {
        return new EsParameterMetaData(this);
    }

    public void setRowId(int parameterIndex, RowId x) throws SQLException {
    }

    public void setNString(int parameterIndex, String value) throws SQLException {
        throw new SQLFeatureNotSupportedException("NString not supported");
    }

    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("NCharacterStream not supported");
    }

    public void setNClob(int parameterIndex, NClob value) throws SQLException {
    }

    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("Clob not supported");
    }

    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("Blob not supported");
    }

    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("NClob not supported");
    }

    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
    }

    public void setObject(int parameterIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
    }


    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("AsciiStream not supported");
    }

    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("BinaryStream not supported");
    }

    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new SQLFeatureNotSupportedException("CharacterStream not supported");
    }

    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        throw new SQLFeatureNotSupportedException("AsciiStream not supported");
    }

    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
        throw new SQLFeatureNotSupportedException("BinaryStream not supported");
    }

    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
        throw new SQLFeatureNotSupportedException("CharacterStream not supported");
    }

    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        throw new SQLFeatureNotSupportedException("NCharacterStream not supported");
    }

    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        throw new SQLFeatureNotSupportedException("Clob not supported");
    }

    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        throw new SQLFeatureNotSupportedException("Blob not supported");
    }

    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        throw new SQLFeatureNotSupportedException("NClob not supported");
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public int executeUpdate(String sql) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public boolean execute(String sql) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public boolean execute(String sql, String[] columnNames) throws SQLException {
        throw new SQLException("Forbidden method on PreparedStatement");
    }

    public long executeLargeUpdate() throws SQLException {
        throw new SQLFeatureNotSupportedException("Batching not supported");
    }
}
