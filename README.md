#### 介绍
个人实现 elasticsearch的jdbc驱动，略微参考官方 x-pack-sql-jdbc (7.6.2，其他版本未测试)，相当于精简官方jdbc，同时又绕过权限问题

#### 源码
[https://gitee.com/wsitm/elasticsearch-jdbc.git](https://gitee.com/wsitm/elasticsearch-jdbc.git)

#### 引用
自行使用源码打包，或
![jar包](https://img-blog.csdnimg.cn/42ecd2f7f48d486ba8d5b91fc4e6feb7.png)
取pack目录下我打包好的jar文件。然后放在在spingboot项目下

![jar包目录](https://img-blog.csdnimg.cn/1e097e9d2283415db4a823d3d85ed0b9.png)
选中“elasticsearch-jdbc-1.0.0-SNAPSHOT.jar”，右键-> Add as Library。
**其实比较建议自己构建一个私有仓库再引用，相对方便。这个是个人兴趣，所有我就不发布到maven仓库了，太麻烦**

**另外，该工具使用fastjson作核心数据处理，必须在 pom 引用**
```xml
        <!-- https://mvnrepository.com/artifact/com.alibaba/fastjson -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.78</version>
        </dependency>
```

#### 配置
+ yml配置文件
```yaml
spring:
  datasource:
    driver-class-name: stu.elasticsearch.jdbc.EsDriver
    url: jdbc:es://192.168.0.1:9200/
    username: xxx
    password: xxxxxx
    hikari:
      read-only: true #该配置为必须配置
      minimum-idle: 3
      max-lifetime: 1800000
      connection-test-query: select 1
mybatis:
  mapper-locations: classpath*:mapper/*Mapper.xml
```

+ mapper接口
```java
@Mapper
public interface EsMapper {
    List<Map<String, Object>> selectList();
}
```

+ mapper 写SQL的xml文件，**其中表名必须使用双引号包含**
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" 
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="pers.xxx.es.mapper.EsMapper">
  <select id="selectList" resultType="java.util.Map">
      SELECT * FROM \"table-*\" LIMIT 10
  </select>
</mapper>
```
**没有任何多余的配置，就和查其他数据库一样**

#### 测试
+ 测试代码
```java
@SpringBootTest(classes = EsApplication.class)
class EsApplicationTests {

    @Autowired
    private EsMapper esMapper;

    @Test
    void test() {
        List<Map<String, Object>> list = esMapper.selectList();
        list.forEach(map -> {
            map.forEach((k, v) -> {
                System.out.print(k + ": " + v + ", ");
            });
            System.out.println("\n");
        });
    }
}
```
+ 测试结果
![测试结果](https://img-blog.csdnimg.cn/4ffeac6449a74a9aab234bca2f27b835.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5pep5pmo6Ziz5YWJ5LiA6Iis5pqW,size_20,color_FFFFFF,t_70,g_se,x_16)
